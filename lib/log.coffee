winston = require 'winston'

module.exports = (module, level) ->
    # Метка с именем файла, который выводит сообщение
    path = module.filename.split('/').slice(-2).join('/')

    # Инициализация логера
    log = new winston.Logger
        transports: [
            new winston.transports.Console
                colorize : yes
                level    : level ? 'debug'
                label    : path
        ]

    # Вывод информации о режиме логирования для текущего модуля
    log.info "(log)"
    log.debug "(!) dedug regimen"

    # Предоставление логера
    log
nconf = require 'nconf'

nconf.argv().env []

nconf.defaults
    server:
        IP   : '0.0.0.0'
        port : 8081

module.exports = nconf
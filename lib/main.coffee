express = require 'express'
fs      = require 'fs'
path    = require 'path'
config  = require './config'
log     = require('./log') module#, 'debug'
env     = {}
am      = {}

# #### Setup the environment variables
env.ip   = config.get 'OPENSHIFT_NODEJS_IP'
env.port = (config.get 'OPENSHIFT_NODEJS_PORT') or config.get 'server:port'

if typeof env.ip is 'undefined'
    #  Log errors on OpenShift but continue with default IP
    log.warn 'No OPENSHIFT_NODEJS_IP var, using %s', config.get 'server:IP'
    env.ip = config.get 'server:IP'

###*
#  terminator === the termination handler
#  Terminate server on receipt of the specified signal.
#  @param {string} sig  Signal to terminate on.
###
am.terminator = (sig) ->
    if typeof sig is 'string'
        log.warn '%s: Received %s - terminating sample app ...', Date( Date.now() ), sig
        process.exit 1
    log.log '%s: Node server stopped.', Date( Date.now() )

# #### Setup termination handlers

# Process on exit and signals.
process.on 'exit', -> am.terminator()

# Removed 'SIGPIPE' from the list - bugz 852598.
[ 'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
].forEach (element, index, array) ->
    process.on element, -> am.terminator element

am.allowCrossDomain = (req, res, next) ->
    res.header 'Access-Control-Allow-Headers', 'Content-Type'
    res.header 'Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE'
    res.header 'Access-Control-Allow-Origin' , '*'
    next()

app = express()
# app.use express.favicon()
app.use express.logger 'dev'
app.use express.bodyParser()
app.use express.methodOverride()
app.use am.allowCrossDomain
app.disable 'etag'
app.use app.router

# Запуск статического файлового сервера, который смотрит на папку ROOT/out/
app.use express.static( path.join __dirname, '../../dsht-app/out' )

# Общая обработка случая отсутствия запрошенного ресурса (ошибка 404)
app.use (req, res, next) ->
    res.status 404
    log.debug  'Not found URL: %s', req.url
    res.send   error: 'Not found'

# Общая обработка случая внутренней ошибки (ошибка 500)
app.use (err, req, res, next) ->
    res.status err.status || 500
    log.error 'Internal error(%d): %s', res.statusCode, err.message
    log.debug 'STACK:', err.stack if err.stack
    res.send   error: err.message

getWarningHTML = (message) -> '<!doctype html>
    <html lang="en">
        <head>
          <meta charset="utf-8">
          <title>Warning</title>
        </head>
        <body>
            <p>
                <strong>Attention!</strong> ' + message + '
            </p>
        </body>
    </html>'

warningAPIRequester = (req, res) ->
    res.setHeader 'Content-Type', 'text/html'
    res.status 404
    res.send getWarningHTML '
    Use <a href="/api/1">existant API:</a>
    <ul>
        <li><a href="/api/v1/images">Images</a></li>
        <li><a href="/api/v1/imagesChunk/:imageId">Chunk with 10 images from imageId</a></li>
    </ul>'

warningURLRequester = (req, res) ->
    res.setHeader 'Content-Type', 'text/html'
    res.status 404
    res.send getWarningHTML '<strong>404</strong> Not Found'

# Расширенная маршрутизация запросов
routes = [
    GET:
        '/api': warningURLRequester
        '/api/': warningURLRequester
        '/api/1': warningURLRequester
        '/images': warningURLRequester
        '/api/1/images': warningURLRequester
]

routes = routes.concat(
    require './api/images'
    require './api/imagesChunk'
)

for routeOptions in routes

    if routeOptions.GET
        for route, callback of routeOptions.GET
            app.get route, callback 
            log.debug route, 'GET'

    if routeOptions.route
        for method, callback of routeOptions.methods
            app[method] routeOptions.route, callback 
            log.debug routeOptions.route, method.toUpperCase()

app.get '/errorExample', (req, res, next) ->
    next new Error 'Just error!'
    
app.listen env.port, env.ip, ->
    log.info '%s: Node server started on %s:%d ...', Date( Date.now() ), env.ip, env.port
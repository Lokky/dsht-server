module.exports =

    withStatus: (httpStatus, message) ->
        err = new Error message
        err.status = httpStatus ? 500
        err

    withError: (err, status) ->
        err.status ?= 500
        err.status = status if status
        err

    asNextWithStatus: (next, httpStatus, message) ->
        err = new Error message
        err.status = httpStatus ? 500
        next err

    asNext: (next, err, client, done, status) ->
        return no if not err
        done client if client
        err.status ?= 500
        err.status = status if status
        next err
        yes